# Overview #

**CommandLineTimer** is a tool I can't imagine my life without, I hope you'll like it too.

Whenever you need to be reminded of something, or you have to do something later, just start this program with command line parameters.
For my command line I have alias: `timer == CommandLineTimer.exe -m`, this is how I use it:

`timer 2h`

`timer until 9:30`

`timer 1h 35m -d"Call somebody"`

---

## [Download](https://bitbucket.org/DimaStefantsov/commandlinetimer/src) CommandLineTimer. ##

You can also compile it yourself from the [source code](https://bitbucket.org/DimaStefantsov/commandlinetimer/src).

---

##Documentation

### Working with time:

    timer 2d 4h 8m 16s
    timer 2 days 4 hours 8 minutes 16 seconds
    timer until 13:55
    timer until tomorrow 00:30
    timer u 13:00
    timer to 13:00
    timer 13:00
    etc.
*Application parses time using .NET Framework built-in methods, so it's pretty flexible, just try to enter time the way you like.*

### Available command line parameters:

**Application will be minimized to tray on startup:**

-c | -close | -closeonstartup | -m | -minimize | -minimizeonstartup | -minimise | -minimiseonstartup | -mini | -min | -tray | -gotray | -gototray

---

**Add description to timer:**

-dTextWithNoSpaces | -d"Text with spaces"

---

**Close all currently running timers:**

-ka | -killall | -killalltimers | -ca | -closeall | -closealltimers