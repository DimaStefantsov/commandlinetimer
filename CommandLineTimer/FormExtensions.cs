﻿using System.Runtime.InteropServices;

namespace System.Windows.Forms
{
    public static class FormExtensions
    {
        [DllImport("user32.dll")]
        private static extern int ShowWindow(IntPtr hWnd, uint Msg);
        private const uint SW_RESTORE = 0x09;
        public static void Restore(this Form form)
        {
            if (form.WindowState == FormWindowState.Minimized)
            {
                ShowWindow(form.Handle, SW_RESTORE);
            }
        }

        [DllImport("user32.dll")]
        static extern bool FlashWindow(IntPtr hwnd, bool bInvert);
        public static void FlashTaskbar(this Form form)
        {
            FlashWindow(form.Handle, false);
        }
    }
}