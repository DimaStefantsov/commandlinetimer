using System;
using System.Text;
using System.Windows.Forms;

namespace CommandLineTimer
{
    public class CountdownTimer
    {
        #region Constants

        private const int maxProgress = 10000;

        #endregion

        #region Fields

        private readonly DateTime expireMoment;
        private readonly TimeSpan total;
        private Timer timer;

        #endregion

        #region Events

        public event EventHandler TimerExpired;

        #endregion

        #region Properties

        public string TimeLeft
        {
            get
            {
                string timeLeft;
                if (IsExpired)
                {
                    timeLeft = "Timer expired";
                }
                else
                {
                    timeLeft = ToString(Remaining);
                }

                return timeLeft;
            }
        }

        public string TimeLeftShort
        {
            get
            {
                string timeLeft;
                if (IsExpired)
                {
                    timeLeft = "Expired";
                }
                else
                {
                    timeLeft = ToStringShort(Remaining);
                }

                return timeLeft;
            }
        }

        /// <summary>
        /// From 0 to 10000
        /// </summary>
        public int Progress
        {
            get
            {
                int progress;
                if (IsExpired)
                {
                    progress = maxProgress;
                }
                else
                {
                    TimeSpan elapsed = total - Remaining;
                    long possiblyNegativeOrEnormousProgress = maxProgress*elapsed.Ticks/total.Ticks;
                    long possiblyNegativeProgress = Math.Min(maxProgress, possiblyNegativeOrEnormousProgress);
                    progress = (int)Math.Max(0, possiblyNegativeProgress);
                }

                return progress;
            }
        }

        public bool IsExpired
        {
            get
            {
                return DateTime.Now > expireMoment;
            }
        }

        private TimeSpan Remaining
        {
            get
            {
                return expireMoment - DateTime.Now;
            }
        }

        #endregion

        #region Constructors

        public CountdownTimer(DateTime expireMoment)
        {
            this.expireMoment = expireMoment;
            total = expireMoment - DateTime.Now;
            InitializeTimer();
        }

        public CountdownTimer(TimeSpan total)
        {
            this.total = total;
            expireMoment = DateTime.Now + total;
            InitializeTimer();
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Use this method to start CountdownTimer.
        /// </summary>
        public void StartExpirationTimer()
        {
            double possiblyNegativeOrZeroInterval = Math.Min(int.MaxValue, Remaining.TotalMilliseconds);
            double validInterval = Math.Max(1, possiblyNegativeOrZeroInterval);
            timer.Interval = (int)validInterval;
            timer.Start();
        }

        #endregion

        #region Helpers

        private void InitializeTimer()
        {
            timer = new Timer();
            timer.Tick += TimerOnTick;
        }

        private void TimerOnTick(object sender, EventArgs eventArgs)
        {
            timer.Stop();
            if (IsExpired)
            {
                OnTimerExpired();
            }
            else
            {
                StartExpirationTimer();
            }
        }

        private void OnTimerExpired()
        {
            var safeEvent = TimerExpired;
            if (safeEvent != null)
            {
                safeEvent(this, new EventArgs());
            }
        }

        /// <summary>
        /// This method have been taken from here
        /// http://www.orzeszek.org/dev/timer/
        /// </summary>
        private static string ToString(TimeSpan ts)
        {
            StringBuilder sb = new StringBuilder();

            if ((int)ts.TotalDays == 1)
                sb.AppendFormat("{0:D}\u00A0day ", (int)ts.TotalDays);
            else if ((int)ts.TotalDays != 0)
                sb.AppendFormat("{0:D}\u00A0days ", (int)ts.TotalDays);

            if ((int)ts.TotalHours == 1)
                sb.AppendFormat("{0:D}\u00A0hour ", ts.Hours);
            else if ((int)ts.TotalHours != 0)
                sb.AppendFormat("{0:D}\u00A0hours ", ts.Hours);

            if ((int)ts.TotalMinutes == 1)
                sb.AppendFormat("{0:D}\u00A0minute ", ts.Minutes);
            else if ((int)ts.TotalMinutes != 0)
                sb.AppendFormat("{0:D}\u00A0minutes ", ts.Minutes);

            if (ts.Seconds == 1)
                sb.AppendFormat("{0:D}\u00A0second", ts.Seconds);
            else
                sb.AppendFormat("{0:D}\u00A0seconds", ts.Seconds);

            return sb.ToString();
        }

        private static string ToStringShort(TimeSpan ts)
        {
            StringBuilder sb = new StringBuilder();

            if ((int)ts.TotalDays == 1)
                sb.AppendFormat("{0:D}d ", (int)ts.TotalDays);
            else if ((int)ts.TotalDays != 0)
                sb.AppendFormat("{0:D}d ", (int)ts.TotalDays);

            if ((int)ts.TotalHours == 1)
                sb.AppendFormat("{0:D}h ", ts.Hours);
            else if ((int)ts.TotalHours != 0)
                sb.AppendFormat("{0:D}h ", ts.Hours);

            if ((int)ts.TotalMinutes == 1)
                sb.AppendFormat("{0:D}m ", ts.Minutes);
            else if ((int)ts.TotalMinutes != 0)
                sb.AppendFormat("{0:D}m ", ts.Minutes);

            if (ts.Seconds == 1)
                sb.AppendFormat("{0:D}s", ts.Seconds);
            else
                sb.AppendFormat("{0:D}s", ts.Seconds);

            return sb.ToString();
        }

        #endregion
    }
}