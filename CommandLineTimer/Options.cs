namespace CommandLineTimer
{
    public class Options
    {
        public string Description { get; set; }
        public bool CloseOnStartup { get; set; }
        public bool KillAllTimers { get; set; }
    }
}