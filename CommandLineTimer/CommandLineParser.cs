﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using CommandLineTimer.Properties;

namespace CommandLineTimer
{
    public static class CommandLineParser
    {
        #region Public methods

        public static Tuple<Options, CountdownTimer> ParseCommandLineArguments(string[] args)
        {
            var optionsArguments = new List<string>();
            var sbTimerArguments = new StringBuilder();
            foreach (string argument in args)
            {
                if (argument[0] == '-' ||
                    argument[0] == '/' ||
                    argument[0] == '\\')
                {
                    optionsArguments.Add(argument.Substring(1));
                }
                else
                {
                    sbTimerArguments.Append(argument + " ");
                }
            }
            string timerArguments = sbTimerArguments.ToString();

            var options = GetOptions(optionsArguments);
            var countdownTimer = GetCountdownTimer(timerArguments);

            return Tuple.Create(options, countdownTimer);
        }

        #endregion

        #region Helpers

        private static Options GetOptions(IEnumerable<string> optionsArguments)
        {
            var result = new Options();
            foreach (string argument in optionsArguments)
            {
                if (string.IsNullOrEmpty(argument))
                {
                    continue;
                }

                string argumentLowercase = argument.ToLowerInvariant();
                if (argumentLowercase[0] == 'd')
                {
                    result.Description = argument.Substring(1);
                }
                else if (argumentLowercase == "c" ||
                         argumentLowercase == "close" ||
                         argumentLowercase == "closeonstartup" ||
                         argumentLowercase == "m" ||
                         argumentLowercase == "minimize" ||
                         argumentLowercase == "minimizeonstartup" ||
                         argumentLowercase == "minimise" ||
                         argumentLowercase == "minimiseonstartup" ||
                         argumentLowercase == "mini" ||
                         argumentLowercase == "min" ||
                         argumentLowercase == "tray" ||
                         argumentLowercase == "gotray" ||
                         argumentLowercase == "gototray")
                {
                    result.CloseOnStartup = true;
                }
                else if (argumentLowercase == "ka" ||
                         argumentLowercase == "killall" ||
                         argumentLowercase == "killalltimers" ||
                         argumentLowercase == "ca" ||
                         argumentLowercase == "closeall" ||
                         argumentLowercase == "closealltimers")
                {
                    result.KillAllTimers = true;
                }
            }

            return result;
        }

        private static CountdownTimer GetCountdownTimer(string timerRelatedArguments)
        {
            CountdownTimer result;

            object o = GetTimeFromString(timerRelatedArguments);
            if (o == null)
            {
                result = new CountdownTimer(DateTime.Now);
            }
            else if (o is DateTime)
            {
                result = new CountdownTimer((DateTime)o);
            }
            else if (o is TimeSpan)
            {
                result = new CountdownTimer((TimeSpan)o);
            }
            else
            {
                result = new CountdownTimer(DateTime.Now);
            }

            return result;
        }

        /// <summary>
        /// This parse method have been taken from here
        /// http://www.orzeszek.org/dev/timer/
        /// </summary>
        private static object GetTimeFromString(string s)
        {
            if (string.IsNullOrEmpty(s))
                return TimeSpan.Zero;

            double minutes;
            if (double.TryParse(s, out minutes))
                return TimeSpan.FromMinutes(minutes);

            DateTime dateTime;
            if (DateTime.TryParse(s, CultureInfo.CurrentCulture, DateTimeStyles.None, out dateTime) &&
                dateTime > DateTime.Now)
                return dateTime;

            bool isDateTime = false;
            bool isDateTimeTomorrow = false;
            
            if (s.StartsWith("until tomorrow", StringComparison.CurrentCultureIgnoreCase))
            {
                s = s.Substring("until tomorrow".Length);
                isDateTime = true;
                isDateTimeTomorrow = true;
            }
            else if (s.StartsWith("until ", StringComparison.CurrentCultureIgnoreCase))
            {
                s = s.Substring("until ".Length);
                isDateTime = true;
            }
            else if (s.StartsWith("u ", StringComparison.CurrentCultureIgnoreCase))
            {
                s = s.Substring("u ".Length);
                isDateTime = true;
            }
            else if (s.StartsWith("to ", StringComparison.CurrentCultureIgnoreCase))
            {
                s = s.Substring("to ".Length);
                isDateTime = true;
            }

            if (isDateTime)
            {
                if (DateTime.TryParse(s, CultureInfo.CurrentCulture, DateTimeStyles.None, out dateTime))
                {
                    return isDateTimeTomorrow ? dateTime.AddDays(1) : dateTime;
                }

                // @TODO: Parse other date formats.

                return null;
            }

            try
            {
                TimeSpan ts = TimeSpan.Zero;
                string[] parts = Regex.Split(s, @"(?<=\d+\s*[a-zA-Z]+)\s*(?=\d+\s*[a-zA-Z]+)");
                foreach (string p in parts)
                    if (!string.IsNullOrEmpty(p))
                    {
                        string[] subparts = Regex.Split(p, @"(?<=\d+)\s*(?=[a-zA-Z]+)");
                        if (subparts.Length == 2)
                            switch (subparts[1].ToLower()[0])
                            {
                                case 'd':
                                    ts = ts.Add(TimeSpan.FromDays(double.Parse(subparts[0])));
                                    break;
                                case 'h':
                                    ts = ts.Add(TimeSpan.FromHours(double.Parse(subparts[0])));
                                    break;
                                case 'm':
                                    ts = ts.Add(TimeSpan.FromMinutes(double.Parse(subparts[0])));
                                    break;
                                case 's':
                                    ts = ts.Add(TimeSpan.FromSeconds(double.Parse(subparts[0])));
                                    break;
                                default:
                                    return null;
                            }
                        else
                            return null;
                    }

                return ts;
            }
            catch (Exception)
            {
            }

            try
            {
                TimeSpan ts = TimeSpan.Zero;
                string[] parts =
                    s.Split(new char[] {'.', ':', '-', ' ', '/', '\\', '\f', '\n', '\r', '\t', '\v', '\x85'},
                            StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < parts.Length; i++)
                {
                    double d = double.Parse(parts[i]);
                    switch (parts.Length - i)
                    {
                        case 1:
                            ts = ts.Add(TimeSpan.FromSeconds(d));
                            break;
                        case 2:
                            ts = ts.Add(TimeSpan.FromMinutes(d));
                            break;
                        case 3:
                            ts = ts.Add(TimeSpan.FromHours(d));
                            break;
                        case 4:
                            ts = ts.Add(TimeSpan.FromDays(d));
                            break;
                        default:
                            return null;
                    }
                }

                return ts;
            }
            catch (Exception)
            {
            }

            TimeSpan timeSpan = TimeSpan.Zero;
            if (TimeSpan.TryParse(s, out timeSpan))
                return timeSpan;

            return null;
        }

        #endregion
    }
}
