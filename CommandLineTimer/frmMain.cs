﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using CommandLineTimer.Properties;

namespace CommandLineTimer
{
    public partial class frmMain : Form
    {
        #region Fields

        private readonly CountdownTimer countdownTimer;
        private readonly Options options;
        private readonly SoundPlayer soundPlayer = new SoundPlayer(Resource.Quiet_beep);
        private bool forceClose;

        #endregion

        #region Constructors

        public frmMain(string[] args)
        {
            InitializeComponent();

            var parseResults = CommandLineParser.ParseCommandLineArguments(args);
            options = parseResults.Item1;
            ActOnOptions();

            countdownTimer = parseResults.Item2;
            StartCountdown();
        }

        #endregion

        #region Event handlers

        private void CountdownTimerOnTimerExpired(object sender, EventArgs eventArgs)
        {
            UpdateInterface();
            StopInterfaceUpdates();

            ShowTimerWindow();
            this.FlashTaskbar();
            soundPlayer.Play();
        }

        private void OpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowTimerWindow();
        }

        private void CloseTimerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            forceClose = true;
            this.Close();
        }

        private void timerInterfaceUpdate_Tick(object sender, EventArgs e)
        {
            UpdateInterface();
        }

        private void frmMain_Resize(object sender, EventArgs e)
        {
            float sizeFactor = Math.Min(Height/150f, Width/350f);
            float fontSize = Math.Max(1, sizeFactor*14);
            lblTimeLeft.Font = new Font(lblTimeLeft.Font.FontFamily, fontSize);
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!forceClose && 
                !countdownTimer.IsExpired)
            {
                HideTimerWindow();
                e.Cancel = true;
            }
        }

        private void notifyIcon_DoubleClick(object sender, EventArgs e)
        {
            ShowTimerWindow();
        }

        private void frmMain_Shown(object sender, EventArgs e)
        {
            if (options.CloseOnStartup)
            {
                this.Close();
            }
        }

        #endregion

        #region Helpers

        private void UpdateInterface()
        {
            progressBar.Value = countdownTimer.Progress;
            lblTimeLeft.Text = countdownTimer.TimeLeft;

            if (string.IsNullOrEmpty(options.Description))
            {
                this.Text = countdownTimer.TimeLeft;
                notifyIcon.Text = countdownTimer.TimeLeft;
            }
            else
            {
                this.Text = options.Description + " - " + countdownTimer.TimeLeft;

                const int MaxTrayTooltipLength = 63;  // Tray tooltip can't contain more than 63 characters.
                string description = options.Description;
                string timeLeft = countdownTimer.TimeLeft;
                if (description.Length + Environment.NewLine.Length + timeLeft.Length > MaxTrayTooltipLength)
                {
                    timeLeft = countdownTimer.TimeLeftShort;
                    if (description.Length + Environment.NewLine.Length + timeLeft.Length > MaxTrayTooltipLength)
                    {
                        int descriptionLength = MaxTrayTooltipLength - timeLeft.Length - Environment.NewLine.Length - 1;
                        description = description.Substring(0, descriptionLength) + "…";
                    }
                }
                notifyIcon.Text = description + Environment.NewLine + timeLeft;
            }
        }

        private void StopInterfaceUpdates()
        {
            timerInterfaceUpdate.Stop();
        }

        private void ShowTimerWindow()
        {
            SetHighInterfaceUpdateRate();

            this.Show();
            this.Restore();
            this.Activate();
        }

        private void HideTimerWindow()
        {
            this.Hide();
            SetDecreasedInterfaceUpdateRate();
        }

        private void SetHighInterfaceUpdateRate()
        {
            timerInterfaceUpdate.Interval = 50;
        }

        private void SetDecreasedInterfaceUpdateRate()
        {
            timerInterfaceUpdate.Interval = 1000;
        }

        private void ActOnOptions()
        {
            if (options.KillAllTimers)
            {
                KillAllTimers();
            }
        }

        private void KillAllTimers()
        {
            var currentProcess = Process.GetCurrentProcess();
            var timers = Process.GetProcessesByName(currentProcess.ProcessName);
            foreach (var timerProcess in timers)
            {
                if (timerProcess.Id != currentProcess.Id)
                {
                    // Long story short:
                    // it's either Mutex or complicated winapi hacks to close other timers gently.
                    timerProcess.Kill();
                }
            }
            notifyIcon.Visible = false; // Try to cleanup a bit.
            notifyIcon.Dispose();
            currentProcess.Kill();
        }

        private void StartCountdown()
        {
            countdownTimer.TimerExpired += CountdownTimerOnTimerExpired;
            countdownTimer.StartExpirationTimer();
            timerInterfaceUpdate.Start();
        }

        #endregion
    }
}
