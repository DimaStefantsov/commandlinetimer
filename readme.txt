CommandLineTimer

For more instructions, visit http://stefantsov.com/projects/commandlinetimer

Example usage:
timer 2d 4h 8m 16s
timer 2 days 4 hours 8 minutes 16 seconds
timer until 13:55
timer until tomorrow 00:30
timer u 13:00
timer to 13:00
timer 13:00

Available command line parameters:
-c | -close | -closeonstartup | -m | -minimize | -minimizeonstartup | -minimise | -minimiseonstartup | -mini | -min | -tray | -gotray | -gototray
-dTextWithNoSpaces | -d"Text with spaces"
-ka | -killall | -killalltimers | -ca | -closeall | -closealltimers

Source code at https://bitbucket.org/DimaStefantsov/commandlinetimer
Made by Dima Stefantsov http://stefantsov.com/